import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  generateRandomObject(): Promise<any> {
    return this.appService.saveRandomObject();
  }

  @Get('/list')
  generateRandomObjects(): Promise<any> {
    return this.appService.saveListOfRandomObjects();
  }
}

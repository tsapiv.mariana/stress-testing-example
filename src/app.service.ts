import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

class RandomTodo {}

@Injectable()
export class AppService {
  constructor(
    @InjectModel('RandomTodo')
    protected randomTodoModel: Model<RandomTodo>,
  ) {}

  titles = ['Random Title 1', 'Random Title 2', 'Random Title 3'];
  descriptions = [
    'Random Description 1',
    'Random Description 2',
    'Random Description 3',
  ];

  saveRandomObject() {
    const randomObject = this.generateRandomObject();
    return this.randomTodoModel.create(randomObject);
  }

  saveListOfRandomObjects() {
    const randomObjects = this.generateBunchOfObjects();
    return this.randomTodoModel.create(randomObjects);
  }

  private generateRandomObject() {
    const randomTitle =
      this.titles[Math.floor(Math.random() * this.titles.length)];
    const randomDescriptions =
      this.descriptions[Math.floor(Math.random() * this.descriptions.length)];

    return {
      date: new Date(),
      title: randomTitle,
      description: randomDescriptions,
    };
  }

  private generateBunchOfObjects() {
    const arr = [];
    for (let i = 0; i < 10; i++) {
      arr.push(this.generateRandomObject());
    }
    return arr;
  }
}

FROM node:14.15 AS server

#  Navigate to the container working directory
WORKDIR /usr/src/app
#  Copy package.json
COPY package*.json ./

RUN npm install
COPY . .
RUN npm run build
